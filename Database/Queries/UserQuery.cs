﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
using api.Models;
using MySql.Data.MySqlClient;

namespace api.Database.Queries
{
    public class UserQuery
    {

        public AppDb Db { get; }

        public UserQuery(AppDb db)
        {
            Db = db;
        }

        public async Task<User> FindById(int id)
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM `users` WHERE `id` = @id;";
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@id",
                DbType = DbType.Int32,
                Value = id,
            });
            var result = await ReadAll(await cmd.ExecuteReaderAsync());
            return result.Count > 0 ? result[0] : null;
        }

        public async Task<List<User>> AllRows()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM `users` ORDER BY `id` DESC;";
            return await ReadAll(await cmd.ExecuteReaderAsync());
        }

        //public async Task DeleteAll()
        //{
        //    using var txn = await Db.Connection.BeginTransactionAsync();
        //    using var cmd = Db.Connection.CreateCommand();
        //    cmd.CommandText = @"DELETE FROM `users`";
        //    await cmd.ExecuteNonQueryAsync();
        //    await txn.CommitAsync();
        //}

        private async Task<List<User>> ReadAll(DbDataReader reader)
        {
            var items = new List<User>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var item = new User(Db)
                    {
                        Id = reader.GetInt32(0),
                        FirstName = reader.GetString(1),
                        MiddleName = reader.GetString(2),
                        LastName = reader.GetString(3),
                        Email = reader.GetString(6),
                        Password = reader.GetString(7)
                    };
                    items.Add(item);
                }
            }
            return items;
        }
    }
}
