﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
using api.Models;
using MySql.Data.MySqlClient;

namespace api.Database.Queries
{
    public class InternshipQuery
    {
        public AppDb Db { get; }

        public InternshipQuery(AppDb db)
        {
            Db = db;
        }

        public async Task<List<Internship>> AllRows()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM `company_internships` ORDER BY `date_from` DESC;";
            return await ReadAll(await cmd.ExecuteReaderAsync());
        }

        public async Task<List<Internship>> AllRowsByUser(int id)
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM `company_internships` where user_id = @id ORDER BY `date_from` DESC;";
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@id",
                DbType = DbType.Int32,
                Value = id,
            });

            return await ReadAllFromView(await cmd.ExecuteReaderAsync());
        }


        private async Task<List<Internship>> ReadAll(DbDataReader reader)
        {
            var items = new List<Internship>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var item = new Internship(Db)
                    {
                      Id = reader.GetInt32(0),
                      CompanyId = reader.GetInt32(1),
                      Name = reader.GetString(2),
                      Description = reader.GetString(3),
                      DateFrom = reader.GetDateTime(4),
                      DateTo = reader.GetDateTime(5),
                      AvailableSpots = reader.GetInt32(6),
                      Crebo = reader.GetInt32(7),
                      Visible = reader.GetBoolean(8),
                    };
                    items.Add(item);
                }
            }
            return items;
        }

        private async Task<List<Internship>> ReadAllFromView(DbDataReader reader)
        {
            var items = new List<Internship>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var item = new Internship(Db)
                    {
                        Id = reader.GetInt32(0),
                        status = reader.GetString(1),
                        UserId = reader.GetInt32(2),
                        DateFrom = reader.GetDateTime(3),
                        Name = reader.GetString(4)

                    };
                    items.Add(item);
                }
            }
            return items;
        }
    }
}
