﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using api.Models;

namespace api.Database.Queries
{
    public class StudentQuery
    {
        public AppDb Db { get; }

        public StudentQuery(AppDb db)
        {
            Db = db;
        }

        public async Task<Student> FindById(int id)
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM `users` WHERE `id` = @id;";
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@id",
                DbType = DbType.Int32,
                Value = id,
            });
            var result = await ReadAll(await cmd.ExecuteReaderAsync());
            return result.Count > 0 ? result[0] : null;
        }

        public async Task<List<Student>> AllRows()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM `users` ORDER BY `id` DESC;";
            return await ReadAll(await cmd.ExecuteReaderAsync());
        }

        //public async Task DeleteAll()
        //{
        //    using var txn = await Db.Connection.BeginTransactionAsync();
        //    using var cmd = Db.Connection.CreateCommand();
        //    cmd.CommandText = @"DELETE FROM `users`";
        //    await cmd.ExecuteNonQueryAsync();
        //    await txn.CommitAsync();
        //}

        private async Task<List<Student>> ReadAll(DbDataReader reader)
        {
            var items = new List<Student>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var item = new Student(Db)
                    {
                        Id = reader.GetInt32(0),
                        FirstName = reader.GetString(1),
                        MiddleName = reader.GetString(2),
                        LastName = reader.GetString(3),
                        StudentId = reader.GetInt32(4),
                        Crebo = reader.GetInt32(5),
                        Email = reader.GetString(6),
                        Password = reader.GetString(7)
                    };
                    items.Add(item);
                }
            }
            return items;
        }
    }
}
