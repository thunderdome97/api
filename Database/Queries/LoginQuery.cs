﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
using api.Models;
using MySql.Data.MySqlClient;

namespace api.Database.Queries
{
    public class LoginQuery
    {
        public AppDb Db { get; }

        public LoginQuery(AppDb db)
        {
            Db = db;
        }



        public async Task<User> FindByCredentials(User u)
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT `id` FROM `users` WHERE `email` = @email AND `password` = @pass;";
 
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@email",
                DbType = DbType.String,
                Value = u.Email,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@pass",
                DbType = DbType.String,
                Value = u.Password,
            });

            var result = await ReadAll(await cmd.ExecuteReaderAsync());
            return result.Count > 0 ? result[0] : null;
        }



        private async Task<List<User>> ReadAll(DbDataReader reader)
        {
            var items = new List<User>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var item = new User(Db)
                    {
                        Id = reader.GetInt32(0)
                    };
                    items.Add(item);
                }
            }
            return items;
        }
    }
}
