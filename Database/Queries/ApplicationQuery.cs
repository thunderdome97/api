﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using api.Models;

namespace api.Database.Queries
{
    public class ApplicationQuery
    {
        public AppDb Db { get; }

        public ApplicationQuery(AppDb db)
        {
            Db = db;
        }

        public async Task<Application> FindById(int id)
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM `application` WHERE `id` = @id;";
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@id",
                DbType = DbType.Int32,
                Value = id,
            });
            var result = await ReadAll(await cmd.ExecuteReaderAsync());
            return result.Count > 0 ? result[0] : null;
        }

        public async Task<List<Application>> AllRows()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM `application` ORDER BY `id` DESC;";
            return await ReadAll(await cmd.ExecuteReaderAsync());
        }

        //public async Task DeleteAll()
        //{
        //    using var txn = await Db.Connection.BeginTransactionAsync();
        //    using var cmd = Db.Connection.CreateCommand();
        //    cmd.CommandText = @"DELETE FROM `application`";
        //    await cmd.ExecuteNonQueryAsync();
        //    await txn.CommitAsync();
        //}

        private async Task<List<Application>> ReadAll(DbDataReader reader)
        {
            var items = new List<Application>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var item = new Application(Db)
                    {
                        Id = reader.GetInt32(0),
                        UserId = reader.GetInt32(1),
                        InternshipId = reader.GetInt32(2),
                    };
                    items.Add(item);
                }
            }
            return items;
        }
    }
}

