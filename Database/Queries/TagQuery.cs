﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
using api.Models;
using MySql.Data.MySqlClient;

namespace api.Database.Queries
{
    public class TagQuery
    {
        public AppDb Db { get; }

        public TagQuery(AppDb db)
        {
            Db = db;
        }

        public async Task<Tag> FindById(int id)
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM `tags` WHERE `id` = @id;";
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@id",
                DbType = DbType.Int32,
                Value = id,
            });
            var result = await ReadAll(await cmd.ExecuteReaderAsync());
            return result.Count > 0 ? result[0] : null;
        }

        public async Task<List<Tag>> AllRows()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM `tags` ORDER BY `id` DESC;";
            return await ReadAll(await cmd.ExecuteReaderAsync());
        }

        public async Task DeleteAll()
        {
            using var txn = await Db.Connection.BeginTransactionAsync();
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM `tags`";
            await cmd.ExecuteNonQueryAsync();
            await txn.CommitAsync();
        }

        private async Task<List<Tag>> ReadAll(DbDataReader reader)
        {
            var items = new List<Tag>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var item = new Tag(Db)
                    {
                        Id = reader.GetInt32(0),
                        Name = reader.GetString(1),
                        //Crebo = reader.GetString(),
                        
                    };
                    items.Add(item);
                }
            }
            return items;
        }
    }
}
