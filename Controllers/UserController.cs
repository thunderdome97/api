﻿using System.Collections.Generic;
using System.Threading.Tasks;
using api.Database;
using api.Database.Queries;
using api.Models;
using Microsoft.AspNetCore.Mvc;

namespace api.Controllers
{
    [Route("api/user")]
    [ApiController]
    public class UserController : ControllerBase
    {

        public UserController(AppDb db)
        {
            Db = db;
        }
        // GET: api/User
        [HttpGet]
        public IActionResult Get()
        {
            return new UnauthorizedResult();
        }

        // GET: api/User/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            await Db.Connection.OpenAsync();
            var query = new UserQuery(Db);
            var rs = await query.FindById(id);

            if (rs is null)
                return new NotFoundResult();
            return new OkObjectResult(rs);
        }

        // POST: api/User
        [HttpPost]
        public void Post([FromBody] string value)
        {
            //return "value";
        }



        // PUT: api/User/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody]User value)
        {
            await Db.Connection.OpenAsync();
            var query = new UserQuery(Db);
            var rs = await query.FindById(id);
            if (rs is null)
                return new NotFoundResult();
            rs.FirstName = value.FirstName;
            rs.MiddleName = value.MiddleName;
            rs.LastName = value.LastName;
            rs.Email = value.Email;
            rs.Password = value.Password;
            rs.Password = value.Password;

            await rs.Update();
            return new OkObjectResult(rs);
        }



        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }


        public AppDb Db { get; }
    }
}
