﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using api.Database;
using api.Database.Queries;
using api.Models;

namespace api.Controllers
{
    [Route("api/tag")]
    [ApiController]
    public class TagController : Controller
    {
        public TagController(AppDb db)
        {
            Db = db;
        }

        // GET: api/Tag
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            await Db.Connection.OpenAsync();
            var query = new TagQuery(Db);
            var rs = await query.AllRows();

            return new OkObjectResult(rs);
        }

        // GET: api/Tag/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Tag
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Tag/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        public AppDb Db { get; }
    }
}
