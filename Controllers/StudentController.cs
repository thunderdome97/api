﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using api.Database;
using api.Database.Queries;
using api.Models;

namespace api.Controllers
{
    [Route("api/student")]
    [ApiController]
    public class StudentController : Controller
    {
        public StudentController(AppDb db)
        {
            Db = db;
        }

        // GET: api/Student
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            await Db.Connection.OpenAsync();
            var query = new StudentQuery(Db);
            var rs = await query.AllRows();

            return new OkObjectResult(rs);
        }

        // GET: api/Student/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            await Db.Connection.OpenAsync();
            var query = new StudentQuery(Db);
            var rs = await query.FindById(id);

            if (rs is null)
                return new NotFoundResult();
            return new OkObjectResult(rs);
        }

        // POST: api/Student
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]Student value)
        {
            await Db.Connection.OpenAsync();
            value.Db = Db;
            await value.Insert();

            return new OkObjectResult(value);
        }

        // PUT: api/Student/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody]Student value)
        {
            await Db.Connection.OpenAsync();
            var query = new StudentQuery(Db);
            var rs = await query.FindById(id);
            if (rs is null)
                return new NotFoundResult();
            rs.FirstName = value.FirstName;
            rs.MiddleName = value.MiddleName;
            rs.LastName = value.LastName;
            rs.StudentId = value.StudentId;
            rs.Crebo = value.Crebo;
            rs.Email = value.Email;
            rs.Password = value.Password;

            await rs.Update();
            return new OkObjectResult(rs);
        }

        // DELETE: api/Student/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await Db.Connection.OpenAsync();

            var query = new StudentQuery(Db);
            var rs = await query.FindById(id);

            if (rs is null)
                return new NotFoundResult();
            await rs.Delete();
            return new OkResult();
      
        }

        //[HttpDelete]
        //public async Task<IActionResult> DeleteAll()
        //{
        //    await Db.Connection.OpenAsync();
        //    var query = new StudentQuery(Db);
        //    await query.DeleteAll();
        //    return new OkResult();
        //}

        public AppDb Db { get; }
    }
}
