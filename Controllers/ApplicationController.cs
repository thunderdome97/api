﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace api.Controllers
{
    // Application as in sollicitatie.


    [Route("api/app")]
    [ApiController]
    public class ApplicationController : ControllerBase
    {
        // GET: api/Application
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Application/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Application
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Application/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
