﻿using System.Collections.Generic;
using System.Threading.Tasks;
using api.Database;
using api.Database.Queries;
using Microsoft.AspNetCore.Mvc;

namespace api.Controllers
{
    [Route("api/internship")]
    [ApiController]
    public class InternshipController : Controller
    {
        public InternshipController(AppDb db)
        {
            Db = db;
        }

        // GET: api/Student
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            await Db.Connection.OpenAsync();
            var query = new InternshipQuery(Db);
            var rs = await query.AllRows();

            return new OkObjectResult(rs);
        }

        
        [Route("user/{id}")]
        public async Task<IActionResult> Get(int id)
        {
            await Db.Connection.OpenAsync();
            var query = new InternshipQuery(Db);
            var rs = await query.AllRowsByUser(id);

            return new OkObjectResult(rs);
        }

        public AppDb Db { get; }
    }
}
