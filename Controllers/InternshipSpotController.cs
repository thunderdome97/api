﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace api.Controllers
{
    [Route("api/internshipspot")]
    [ApiController]
    public class InternshipSpotController : ControllerBase
    {
        // GET: api/InternshipSpot
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/InternshipSpot/5
        [HttpGet("{id}")] // Name = hoeft niet>?
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/InternshipSpot
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/InternshipSpot/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
