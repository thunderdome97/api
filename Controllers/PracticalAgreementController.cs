﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace api.Controllers
{
    [Route("api/practicalagreement")]
    [ApiController]
    public class PracticalAgreementController : ControllerBase
    {
        // GET: api/PracticalAgreement
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/PracticalAgreement/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/PracticalAgreement
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/PracticalAgreement/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
