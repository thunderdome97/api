﻿using System;
using System.Threading.Tasks;
using api.Database;
using api.Database.Queries;
using api.Models;
using Microsoft.AspNetCore.Mvc;

namespace api.Controllers
{
    [Route("api/login")]
    [ApiController]
    public class LoginController : Controller
    {
        public AppDb Db { get; }

        public LoginController(AppDb db)
        {
            Db = db;
        }

        // POST: api/Login
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]User value)
        {
            await Db.Connection.OpenAsync();
            var query = new LoginQuery(Db);
            var rs = await query.FindByCredentials(value);

            if (rs != null)
                return new OkObjectResult(rs);

            return new UnauthorizedResult();
        }
    }
}
