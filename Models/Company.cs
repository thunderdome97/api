﻿using System;
namespace api.Models
{
    public class Company : User
    {
        public string CompanyName { get; set; }
        public string Description { get; set; }
        public int Validated { get; set; }
        public int Public { get; set; }

        public Company()
        {
        }

        public void AddInternship()
        {

        }

        public void EditInternship(Internship id)
        {

        }

        public void DeleteInternship()
        {

        }


    }
}
