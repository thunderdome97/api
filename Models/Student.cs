using System;
using System.Threading.Tasks;
using api.Database;
using MySql.Data.MySqlClient;


namespace api.Models
{
    public class Student : User
    {
        public int StudentId { get; set; }
        public int ClassId { get; set; }
        public int Crebo { get; set; }

        public Student()
        {
            
        }

        internal Student(AppDb db)
        {
            Db = db;
        }

        public async Task Insert()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"INSERT INTO `users` (`first_name`,`middle_name`,`last_name`,`student_id`,`crebo`,`email`,`password`,`created_at`) VALUES (@firstName, @middleName, @lastName, @studentId, @crebo, @email, @password, NOW());";
            BindParams(cmd);

            await cmd.ExecuteNonQueryAsync();
            Id = (int)cmd.LastInsertedId;
        }

        public async Task Update()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"UPDATE `users` SET `first_name` = @firstName, `middle_name` = @middleName, `last_name` = @lastName, `student_id` = @studentId, `crebo` = @crebo, `email` = @email, `password` = @password, `updated_at` = NOW() WHERE `id` = @id;";
            BindParams(cmd);
            BindId(cmd);

            await cmd.ExecuteNonQueryAsync();
        }


        // Set value of @id param
        private void BindId(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@id",
                DbType = System.Data.DbType.Int32,
                Value = Id,
            });
        }


        // Bind all requested params
        private void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@firstName",
                DbType = System.Data.DbType.String,
                Value = FirstName,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@middleName",
                DbType = System.Data.DbType.String,
                Value = MiddleName,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@lastName",
                DbType = System.Data.DbType.String,
                Value = LastName,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@studentId",
                DbType = System.Data.DbType.Int32,
                Value = StudentId,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@crebo",
                DbType = System.Data.DbType.Int32,
                Value = Crebo,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@email",
                DbType = System.Data.DbType.String,
                Value = Email,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@password",
                DbType = System.Data.DbType.String,
                Value = Password,
            });
        }


    }
}
