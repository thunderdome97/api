﻿using System;
using System.Threading.Tasks;
using api.Database;
using MySql.Data.MySqlClient;

namespace api.Models
{
    public class Application
    {
        public int Id { get; set; }
        public int InternshipId { get; set; }
        public int UserId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Status { get; set; }
        public int Validated { get; set; }
        public int ValidatedBy { get; set; }
        public DateTime ValidatedDate { get; set; }

        internal AppDb Db { get; set; }

        public Application()
        {
        }

        internal Application(AppDb db)
        {
            Db = db;
        }

        public async Task Insert()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"INSERT INTO `application` (`user_id`, `company_internship_id`, `start_date`, `end_date`, `created_at`) VALUES (@userId, @internshipId, @startDate, @endDate, NOW());";
            BindParams(cmd);

            await cmd.ExecuteNonQueryAsync();
            Id = (int)cmd.LastInsertedId;
        }

        public async Task Update()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"UPDATE `application` SET  `user_id` = @userId, `company_internship_id` = @internshipId, `start_date` = @startDate, `end_date` = endDate, `status` = @status, `validated`= @validated, `validated_by`= @validatedBy, `validated_date`=@validatedDate, `updated_at` = NOW() WHERE `id` = @id;";
            BindParams(cmd);
            BindId(cmd);

            await cmd.ExecuteNonQueryAsync();
        }

        public async Task Delete()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM `application` WHERE `id` = @id;";
            BindId(cmd);

            await cmd.ExecuteNonQueryAsync();
        }


        // Set value of @id param
        private void BindId(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@id",
                DbType = System.Data.DbType.Int32,
                Value = Id,
            });
        }

        // Bind all requested params
        private void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@internshipId",
                DbType = System.Data.DbType.Int32,
                Value = InternshipId,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@userId",
                DbType = System.Data.DbType.Int32,
                Value = UserId,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@startDate",
                DbType = System.Data.DbType.DateTime,
                Value = StartDate,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@endDate",
                DbType = System.Data.DbType.DateTime,
                Value = EndDate,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@status",
                DbType = System.Data.DbType.Int32,
                Value = Status,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@validated",
                DbType = System.Data.DbType.Int32,
                Value = Validated,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@validatedBy",
                DbType = System.Data.DbType.Int32,
                Value = ValidatedBy,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@validatedDate",
                DbType = System.Data.DbType.DateTime,
                Value = ValidatedDate,
            });
        }
    }
}
