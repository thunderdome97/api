﻿using System;
using System.Threading.Tasks;
using api.Database;
using MySql.Data.MySqlClient;

namespace api.Models
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        

        internal AppDb Db { get; set; }


        internal User(AppDb db)
        {
            Db = db;
        }


        public User()
        {
        }

        public async Task Insert()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"INSERT INTO `users` (`first_name`,`middle_name`,`last_name`,`email`,`password`,`created_at`) VALUES (@firstName, @middleName, @lastName, @email, @password, NOW());";
            BindParams(cmd);

            await cmd.ExecuteNonQueryAsync();
            Id = (int)cmd.LastInsertedId;
        }

        public async Task Update()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"UPDATE `users` SET `first_name` = @firstName, `middle_name` = @middleName, `last_name` = @lastName, `email` = @email, `password` = @password, `updated_at` = NOW() WHERE `id` = @id;";
            BindParams(cmd);
            BindId(cmd);

            await cmd.ExecuteNonQueryAsync();
        }

        public async Task Delete()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM `users` WHERE `id` = @id;";
            BindId(cmd);

            await cmd.ExecuteNonQueryAsync();
        }


        // Set value of @id param
        private void BindId(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@id",
                DbType = System.Data.DbType.Int32,
                Value = Id,
            });
        }


        // Bind all requested params
        private void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@firstName",
                DbType = System.Data.DbType.String,
                Value = FirstName,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@middleName",
                DbType = System.Data.DbType.String,
                Value = MiddleName,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@lastName",
                DbType = System.Data.DbType.String,
                Value = LastName,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@email",
                DbType = System.Data.DbType.String,
                Value = Email,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@password",
                DbType = System.Data.DbType.String,
                Value = Password,
            });
        }

    }
}