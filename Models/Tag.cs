﻿using System;
using System.Threading.Tasks;
using api.Database;
using MySql.Data.MySqlClient;

namespace api.Models
{
    public class Tag
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Crebo { get; }
        public int Visible { get; set; }
        internal AppDb Db { get; set; }

        // crebo & visible nog in db structure
        public Tag()
        {
        }

        internal Tag(AppDb db)
        {
            Db = db;
        }

        public async Task Insert()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"INSERT INTO `tags` (`name`,`created_at`) VALUES (@tagName, NOW());";
            BindParams(cmd);

            await cmd.ExecuteNonQueryAsync();
            Id = (int)cmd.LastInsertedId;
        }

        public async Task Update()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"UPDATE `tags` SET `name` = @tagName, `updated_at` = NOW() WHERE `id` = @id;";
            BindParams(cmd);
            BindId(cmd);

            await cmd.ExecuteNonQueryAsync();
        }

        public async Task Delete()
        {
            using var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"DELETE FROM `tags` WHERE `id` = @id;";
            BindId(cmd);

            await cmd.ExecuteNonQueryAsync();
        }


        // Set value of @id param
        private void BindId(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@id",
                DbType = System.Data.DbType.Int32,
                Value = Id,
            });
        }


        // Bind all requested params
        private void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@tagName",
                DbType = System.Data.DbType.String,
                Value = Name,
            });
          
        }

    }
}
