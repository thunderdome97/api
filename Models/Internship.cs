﻿using System;
using api.Database;

namespace api.Models
{
    public class Internship
    {
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public int AvailableSpots { get; set; }
        public int Crebo { get; set; }
        public bool Visible { get; set; }

        public int UserId { get; set; }
        public string status { get; set; } 

        internal AppDb Db { get; set; }

        public Internship()
        {
        }

        internal Internship(AppDb db)
        {
            Db = db;
        }


    }
}
