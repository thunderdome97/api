﻿using System;
namespace api.Models
{
    public class Teacher : User
    {
        public string TeacherCode { get; set; }
        public int Location { get; set; }
        public bool Mentor { get; set; }
        public int ClassId { get; set; }

        public Teacher()
        {
        }

        public void GetStudentInfo()
        {

        }

        public void ValidateApplication()
        {

        }

        public void ValidatePA()
        {

        }

        public void ValidateCompany()
        {

        }

        public void AddStudent()
        {

        }
     
    }
}
